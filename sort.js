const list = [30, -5, 0, 10, 5];

function mySort(list) {
  if (list.length < 2) return list;

  let pivot = list.pop();

  return [
    ...mySort(list.filter(element => element <= pivot)),
    pivot,
    ...mySort(list.filter(element => element > pivot))
  ];
}

const result = mySort([...list]);

console.log(result); // [-5, 0, 5, 10, 30]
