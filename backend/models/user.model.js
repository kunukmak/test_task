const mongoose = require("mongoose");
const { Schema } = mongoose;

const UserSchema = new Schema(
  {
    email: { type: String, required: true },
    password: { type: String, required: true }
  },
  {
    timestamps: true
  }
);

UserSchema.index({ email: 1 }, { unique: true });

module.exports = mongoose.model("user", UserSchema);
