const mongoose = require("mongoose");
const { Schema } = mongoose;

const EventSchema = new Schema(
  {
    name: { type: String, required: true },
    date: { type: Date, required: true }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("event", EventSchema);
