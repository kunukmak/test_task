const express = require("express");
const compression = require("compression");
const mongoose = require("mongoose");

const errorMiddleware = require("./middlewares/error");
const authRouter = require("./routes/auth.route");
const eventRouter = require("./routes/event.route");

mongoose
  .connect(
    "mongodb://localhost/testtask",
    { useNewUrlParser: true, reconnectTries: 5 }
  )
  .then(() => {
    const app = express();

    app.use(compression());
    app.use(express.json({ limit: "1mb" }));

    app.use("/auth", authRouter);
    app.use("/events", eventRouter);

    app.use(errorMiddleware.handle404Error);
    app.use(errorMiddleware.handleErrors);

    app.listen(8000, () => {
      console.log("API on port 8000");
    });
  })
  .catch(err => {
    console.error(err);
    process.exit(1);
  });
