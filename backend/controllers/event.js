const mongoose = require("mongoose");
const {
  UnauthorizedError,
  BadRequestError,
  NotFoundError
} = require("../helpers/CustomError");
const EventModel = require("../models/event.model");
const tokens = require("../helpers/tokens");

module.exports = {
  get: (req, res, next) => {
    let searchOptions = {};
    if (res.locals && res.locals.date) {
      searchOptions = {
        date: {
          $gte: res.locals.date,
          $lte: new Date(res.locals.date).setTime(
            res.locals.date.getTime() + 60 * 60 * 24 * 1000
          )
        }
      };
    }

    const [pagesize, page] = [
      parseInt(req.query.pagesize),
      parseInt(req.query.page) || 1
    ];
    !pagesize
      ? EventModel.find(searchOptions)
          .then(events => res.json(events))
          .catch(next)
      : EventModel.find(searchOptions)
          .skip((page - 1) * pagesize)
          .limit(pagesize)
          .then(events => res.json(events))
          .catch(next);
  },

  post: (req, res, next) => {
    const _id = mongoose.Types.ObjectId();
    new EventModel({ _id, name: req.body.name, date: req.body.date })
      .save()
      .then(() => res.status(201).json({ id: _id }));
  },

  put: (req, res, next) => {
    EventModel.findByIdAndUpdate(
      req.params.id,
      {
        name: req.body.name,
        date: req.body.date
      },
      { upsert: true }
    ).then(() => res.json("Succesfully updated event"));
  },

  delete: (req, res, next) => {
    EventModel.findByIdAndDelete(req.params.id).then(() =>
      res.status(204).send()
    );
  }
};
