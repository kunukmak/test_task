const eventRouter = require("express").Router();
const event = require("../controllers/event");
const validations = require("../middlewares/validations");
const checkToken = require("../middlewares/checkToken");
const parseDate = require("../middlewares/parseDate");

eventRouter.get(
  "/",
  validations.eventGet(),
  validations.result,
  checkToken,
  parseDate,
  event.get
);

eventRouter.post(
  "/",
  checkToken,
  validations.eventPost(),
  validations.result,
  event.post
);

eventRouter.put(
  "/:id",
  checkToken,
  validations.eventPut(),
  validations.result,
  event.put
);

eventRouter.delete("/:id", checkToken, event.delete);

module.exports = eventRouter;
