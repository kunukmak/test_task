const list = [10, 20, 30];

function changeElements(list) {
  const element = list.splice(1, 1);
  list.unshift(element[0]);
}

changeElements(list);

console.log(list); // [20, 10, 30];
