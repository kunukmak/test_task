var func1 = function(message) {
  this(message);
};

var func2 = func1.bind(alert);
func2("Test"); // alert 'Test'

function myBind(func, context) {
  const returnValue = { ...func };
  return (returnValue.this = context);
}

var func3 = myBind(func1, alert);
func3("Test"); // alert 'Test'
